import requests
import xml.etree.ElementTree as ET
import json


"""
Retrieve a summary of all NFL games this season for the provided week. Start by
generating a set of games in that week by parsing the XML in endpoint 1. Next
create a list of the game summaries. Since Tornado does not automatically write
lists as JSON, it must first be serialized as a JSON string if valid. Otherwise,
return an error dict, which Tornado automatically writes as JSON, for handling 404s.

@param week - (string) week of regular NFL season to query
@return - (string) a list serialized as JSON string if there are games to summarize
 (dict) error for 404s
"""
def get_games(week):
	url = "http://www.nfl.com/ajax/scorestrip?season=2017&seasonType=REG&week={w}".format(w=week)
	response = requests.get(url)
	tree = ET.fromstring(response.content)
	eids = tree.findall("./gms/g")
	games = games_builder(eids)
	if has_games(games):
		return json.dumps(games)
	else:
		return {"error" : "No games found."}


"""
Checks whether a week has games to summarize.

@param games - (list) games for a given week
@return - (boolean) there are games and that at least one game contains data
"""
def has_games(games):
	if games:
		for g in games:
			if g:
				return True
	return False


"""
Creates a list of NFL games for a given week. Each game is stored as a dict 
representing a JSON object. If there is no data available for a game, the game
will be stored as an empty dict.
For improved performance on subsequent queries of this week, these values should
be stored in a database rather than recreating the game summaries for each query.
This implementation assumes that the only valid states for games are "P" for games 
that haven't been played, "F" for games completed in regulation, and "FO" for games 
that completed in overtime.

* If game data is updated on endpoint 2 as the game happens live, then immediately 
caching into the database will be inefficient as it will need to be updated for every 
query during the game and then again after the game is completed, so the database 
should only update on game completion.
* Storing empty dicts for games that haven't been played yet is a little janky and
ensures has_games is an O(n) operation. While this probably isn't an issue in the scope
of the assignment due to a relatively small number of games each week, this can be
remedied by caching the week into a database before any games.

@param eids - (set) all game ids in a given week
@return - (list) all NFL game data for a given week
"""
def games_builder(eids):
	games = []
	for g in eids:
		if g.attrib["q"] == "P":
			games.append({})
		else:
			games.append(game_stats(g.attrib["eid"], g.attrib["q"] == "FO"))
	return games


"""
Summarizes NFL game with relevant game data as a dict. Data for "home" and "away" 
teams are stored as separate dicts within each game dict. Iterates through all 
passers/receivers/rushers and updates a reference to the top passer/receiver/rusher 
as a new top passer/receiver/rusher is identified. Also adds up all yards 
received/rushed through iteration and stored in the appropriate home/away dict.

* Sum and Max functions may be cleaner than as is currently, but I'm fairly sure there
are no performance benefits.

@param eid - (string) game id
@param overtime - (boolean) the game went to overtime
@return - (dict) {
					"away" : {
								"score",
								"team_abbrv",
								"top_passer",
								"top_receiver",
								"top_rusher",
								"yards_passed",
								"yards_rushed",
							},
					"home" : { ... },
					"overtime" : overtime,
				}
"""
def game_stats(eid, overtime):
	url = "http://www.nfl.com/liveupdate/game-center/{id}/{id}_gtd.json".format(id=eid)
	game = {}
	for team in ["away", "home"]:
		stats = {}
		team_json = requests.get(url).json()[eid][team]
		for position in ["pass", "receiv", "rush"]:
			position_stats = team_json["stats"][position + "ing"]
			stats["score"] = team_json["score"]["T"]
			stats["team_abbrv"] = team_json["abbr"]
			total_yards = 0
			top_yards = 0
			for position_id in position_stats:
				if position_stats[position_id]["yds"] > top_yards:
					top_player = position_stats[position_id]["name"]
					top_yards = position_stats[position_id]["yds"]
				total_yards += position_stats[position_id]["yds"]
			stats["top_" + position + "er"] = top_player
			if position is not "receiv":
				stats["yards_" + position + "ed"] = total_yards
		game[team] = stats
	game["overtime"] = overtime
	return game