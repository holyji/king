import secrets

"""
Valid set of generated keys. Realistically the max set size will never be hit
and verifying that an API key exists in this set shouldn't be hurt performance,
but this should be in a database anyway. And encrypted to be extra safe.
"""
api_keys = {"7ed30c835c845317ea5964ac9ca7b2fb"}

"""
Generates new hashes for API keys. Only relevant in future implementations.
"""
def generate_key():
	new_key = secrets.token_hex(nbytes=16) 
	keys.add(new_key)
	return new_key