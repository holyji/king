Prerequisites:
Python 3.6.3

Installing:
pip install -r requirements.txt

Deployment:
Elastic Beanstalk (load balancing, autoscaling)
eb deploy