import tornado.web
import tornado.wsgi
import re
import games
import keys


class GamesHandler(tornado.web.RequestHandler):

	"""
	Redirects HTTP to HTTPS if necessary
	"""
	def prepare(self):
		if ("X-Forwarded-Proto" in self.request.headers and 
			self.request.headers["X-Forwarded-Proto"] != "https"):
			self.redirect(re.sub(r"^([^:]+)", "https", self.request.full_url()))


	"""
	Handles requests to valid endpoints. Server is secured against unauthorized 
	access via API keys, but user authentication and secret cookies may be necessary 
	depending on the use cases for the server. An ideal implementation of verifying 
	these API keys would allow them to be sent both through queries as well as request 
	headers. However since I wasn't sure how the server would be tested, I chose to 
	demonstrate only verifying query params for the tokens (with a valid default value 
	if not otherwise specified). The code segment commented out below offers a foundation 
	for verifying API keys sent	in request headers by first ensuring the key wasn't sent
	via a query, then returning a 401 if a valid key isn't found in the request header.

	Response codes-
	 200: valid week of games to summarize
	 400: week is not a number or outside of regular season weeks
	 401: unacceptable API key
	 404: no games to summarize
	"""
	def get(self, week):
		
		"""
		if not self.get_arguments("api_key"):
			if "Auth-Token" in self.request.headers:
				api_key = self.request.headers["Auth-Token"]
			else:
				self.send_error(status_code=401)
		elif self.get_argument("api_key") in keys.api_keys:
			api_key = self.get_argument("api_key")
		"""

		api_key = self.get_argument("api_key", default="7ed30c835c845317ea5964ac9ca7b2fb")
		if api_key in keys.api_keys:
			if not week.isdigit():
				self.set_status(400)
				self.write({"error" : "Week must be a number."})
			elif int(week) < 1 or int(week) > 17:
				self.set_status(400)
				self.write({"error" : "Week must be between 1 and 17."})
			else:
				g = games.get_games(week)
				if isinstance(g,str):
					self.set_header("Content-Type", "application/json")
				else:
					self.set_status(404)
				self.write(g)
			self.finish()
		else:
			self.send_error(status_code=401)


"""
Web app must be named "application" and wrapped as WSGI app for use on AWS
"""
application = tornado.web.Application([
	(r"/api/games/(\w+)", GamesHandler),
])
application = tornado.wsgi.WSGIAdapter(application)